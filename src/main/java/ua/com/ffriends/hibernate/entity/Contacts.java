package ua.com.ffriends.hibernate.entity;

import javax.persistence.*;

/**
 * Created by Alex on 12.03.2017.
 */
@Entity
public class Contacts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int iduser;
    private int idcontact;
    private int isfriend;
    private int visibleFor;
    private int users_id;

    @ManyToOne(cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    private Users users;
}
